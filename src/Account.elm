module Account exposing (..)

import Lib exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

type alias Model = { global: Global, user: User }

type Msg = Reset

update : Msg -> Model -> Either Transition Model
update msg { global } = case msg of
  Reset -> Left (ShowLogin global)

view : Model -> Html Msg
view { user } =
  div [ class "container" ]
    [ h3 [] [ text <| "Welcome " ++ user ++ "!" ]
    , button [ class "btn", onClick Reset ] [ text "Reset" ]
    ]
