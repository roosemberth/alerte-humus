module Lib exposing (..)

uncurry : (a -> b -> c) -> (a, b) -> c
uncurry f (a, b) = f a b

type Either a b = Left a | Right b

type alias User = String
type alias Pass = String

-- Application-wide state
type Global = Global { users: List (User, Pass) }

findUserPass : Global -> User -> Maybe Pass
findUserPass (Global { users }) u = users
  |> List.filter (\(lu, _) -> lu == u)
  |> List.map (\(_, p) -> p)
  |> List.head

type Transition =
    Start
  | ShowLogin Global
  | ShowAccount Global User
