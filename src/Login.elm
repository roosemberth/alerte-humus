module Login exposing (..)

import Lib exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

type alias LoginData = { user: User, pass: Pass }
type alias LoginErrs = { user: Maybe String, pass: Maybe String, login: Maybe String }

defaultLoginData : LoginData
defaultLoginData = { user="", pass="" }
defaultLoginErrs : LoginErrs
defaultLoginErrs = { user=Nothing, pass=Nothing, login=Nothing }

type alias Model = { global: Global, data: LoginData, errs: LoginErrs }

dataHasErr : LoginErrs -> Bool
dataHasErr { user, pass, login } = case (user, pass, login) of
  (Nothing, Nothing, Nothing) -> False
  _ -> True

sanitizeUser : (LoginData, LoginErrs) -> (LoginData, LoginErrs)
sanitizeUser (up, e) =
  let user = if up.user |> String.trim |> String.isEmpty
             then Just "The username cannot be empty" else Nothing
  in (up, { e | user = user })

sanitizePass : (LoginData, LoginErrs) -> (LoginData, LoginErrs)
sanitizePass (up, e) =
  let pass = if up.pass |> String.isEmpty
             then Just "The password cannot be empty" else Nothing
  in (up, { e | pass = pass })

clearErrLogin : (LoginData, LoginErrs) -> (LoginData, LoginErrs)
clearErrLogin (up, e) = (up, { e | login = Nothing })

checkPassword : Global -> (LoginData, LoginErrs) -> (LoginData, LoginErrs)
checkPassword g (up, e) = case (findUserPass g up.user) of
  Nothing -> (up, { e | login = Just "Invalid credentials" })
  Just dbp ->
    if dbp == up.pass
    then clearErrLogin (up, e)
    else (up, { e | login = Just "Invalid credentials" })

type Msg =
    Submit
  | UpdateUser String
  | UpdatePass String
  | Reset

tryLogin : Global -> (LoginData, LoginErrs) -> (LoginData, LoginErrs)
tryLogin g t =
  let
    (d, validationErrs) = t
      |> clearErrLogin
      |> sanitizeUser
      |> sanitizePass
  in if dataHasErr validationErrs
     then (d, validationErrs)
     else (d, validationErrs) |> checkPassword g

update : Msg -> Model -> Either Transition Model
update msg { global, data, errs } =
  let toModel (newData, newErrs) =
        Right { global=global, data=newData, errs=newErrs}
  in case msg of
  Submit ->
    let (newData, newErrs) = tryLogin global (data, errs)
    in if dataHasErr newErrs
       then toModel (newData, newErrs)
       else Left (ShowAccount global newData.user)
  (UpdateUser u) -> ({ data | user = u }, errs)
    |> clearErrLogin
    |> sanitizeUser
    |> toModel
  (UpdatePass p) -> ({ data | pass = p }, errs)
    |> clearErrLogin
    |> sanitizePass
    |> toModel
  Reset -> toModel (defaultLoginData, defaultLoginErrs)

view : Model -> Html Msg
view { data, errs } =
  let
    helpMsg f = case f of
      Just msg -> span [ class "help-block" ] [ text msg ]
      Nothing -> div [] []
    maybeErr f = class <| if f == Nothing then "" else "has-error"
  in Html.form [ class "container", onSubmit Submit  ]
    [ h2 [] [ text "Please identify yourself" ]
    , div [ class "form-group", maybeErr errs.user ]
      [ label [ attribute "for" "username" ] [ text "Username" ]
      , input [ class "form-control", type_ "text", onInput UpdateUser
              , placeholder "username", value data.user, id "username" ] []
      , helpMsg errs.user
      ]
    , div [ class "form-group", maybeErr errs.pass ]
      [ label [ attribute "for" "password" ] [ text "Password" ]
      , input [ class "form-control", type_ "password", onInput UpdatePass
              , placeholder "password", value data.pass, id "password" ] []
      , helpMsg errs.pass
      ]
    , input [ class "btn", class "btn-default", type_ "submit", value "Log in"
            , class <| if errs.login == Nothing then "" else "btn-danger"] []
    , button [ class "btn", onClick Reset ] [ text "Cancel" ]
    , div [ maybeErr errs.login ]
      [ helpMsg errs.login ]
    ]

