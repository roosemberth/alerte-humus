module Main exposing (..)

import Lib exposing (..)
import Account
import Login

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

type Model =
    ModelLogin Login.Model
  | ModelAccount Account.Model

runTransition : Transition -> Model
runTransition t =
  let initialGlobal = Global { users = [] }
  in case t of
  Start ->
    ModelLogin { global=initialGlobal
               , data=Login.defaultLoginData
               , errs=Login.defaultLoginErrs
               }
  ShowLogin g ->
    ModelLogin { global=g
               , data=Login.defaultLoginData
               , errs=Login.defaultLoginErrs
               }
  ShowAccount g u -> ModelAccount { global=g, user=u }

type Msg =
    MsgLogin Login.Msg
  | MsgAccount Account.Msg

update : Msg -> Model -> Model
update msg model = case (msg, model) of
  (MsgLogin sMsg, ModelLogin sModel) -> case Login.update sMsg sModel of
    Left t -> runTransition t
    Right m -> ModelLogin m
  (MsgAccount sMsg, ModelAccount sModel) -> case Account.update sMsg sModel of
    Left t -> runTransition t
    Right m -> ModelAccount m
  _ -> runTransition Start -- TODO: Better error handling?

view : Model -> Html Msg
view model = case model of
  ModelLogin m -> Html.map MsgLogin <| Login.view m
  ModelAccount m -> Html.map MsgAccount <| Account.view m

main : Program () Model Msg
main = Browser.sandbox { init = runTransition Start, update = update, view = view }
